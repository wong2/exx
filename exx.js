var fs = require('fs');
var xlsx = require('node-xlsx');
var _ = require("underscore")._;

if (process.argv.length < 5) {
  console.log('Usage: node exx.js input.xlsx sheet output.txt [x y]');
  process.exit();
}

var input_path = process.argv[2]
  , sheet_name = process.argv[3]
  , output_path = process.argv[4]
  , x = process.argv[5] || 1
  , y = process.argv[6] || 1;


var sheets = xlsx.parse(input_path) || [];
var sheet = _.find(sheets, function(sheet) {
  return sheet.name == sheet_name;
});

if (!sheet) {
  console.log('No sheet names ', sheet_name);
  process.exit();
}

var fp = fs.openSync(output_path, 'w');

var rows = sheet.data.slice(x-1);
var lines = rows.map(function(row, m) {
  var cols = row.slice(y-1);
  while (cols.length) {
    if (cols[cols.length-1] !== undefined) {
      break;
    } 
    cols.pop();
  }
  return cols.join('\t');
}).filter(function(row) {
  return !!row.trim();
});

fs.writeSync(fp, lines.join('\n'));
fs.close(fp, function() {
  console.log('Done!');
});
